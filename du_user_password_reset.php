<?php

namespace du {

  /**
   * Output and process user login/registration forms
   *
   * @package default
   * @author mohammad@duable.com
   **/
  class password_reset {
    
    function __construct( $args = array() ){
      # Form field prefixes
      $this->prefix           = 'du_user_login_';

      # Logged in redirect url
      $this->redirect      = '?loggedin';

      # Get arguments
      $this->args = $args;

      # Load AJAX Handler JS
      add_action( 'wp_enqueue_scripts', array( $this, 'load_js' ) );
      
      # Process login/register on init
      //add_action( 'init', array( $this, 'reset_password' ) );
      
      # AJAX Calls
      add_action( 'wp_ajax_password_reset' , array( $this, 'reset_password'  ) );
      add_action( 'wp_ajax_nopriv_password_reset' , array( $this, 'reset_password'  ) );

      $this->forms = new forms;
    }

    function load_js() {
      wp_register_script(
        'du-users-ajax'
        , ( du_site_asset( '/vendor/duable/du-user-registration/assets/js/jquery.du-user-ajax.js' ) )
        , 'global'
        , null
        , true
      );
      wp_enqueue_script( 'du-users-ajax' );
      # Load Ajax Script
      wp_localize_script( 
        'du-users-ajax', 
        'du_users_ajax', 
        array( 
          'ajax_url'  => admin_url( 'admin-ajax.php' ),
          'nonce'     => wp_create_nonce( 'duable' )
        ) 
      );
    }

    function load_css( $css_files ) {
      $css = array( 'du-users-forms' => '/vendor/duable/du-user-registration/assets/css/du-user-login-register-forms.css' );
      array_splice( $css_files, 1, 0, $css );
      return $css_files;
    }

    function password_reset_form( $fields=null ){
      if ( is_user_logged_in() )
        die( 'You are already logged in.' );

      global $wp;
      $current_url = home_url( add_query_arg( array(), $wp->request ) );

      # Default fields
      if ( empty( $fields ) )
        $fields = array( 
          'email_address' => array( 
            'required' => true,
            'type'     => 'email',
            'desc'     => "Email Address"
          ), 
        ); 
      
      # Create a variable with existing values in $_POST
      foreach ( $fields as $field => $options ) {
        $this->current_value[ $field ] = '';
        if ( ( $field != 'password' ) && ( $field != 'repeat_password' ) ) {
          $this->current_value[ $field ] = ( !empty($_POST["du_forgot_password_" . $field] ) ? $_POST["du_forgot_password_" . $field] : '' );
        }
      }

      # Build the form ?>
      
        <?php 
        foreach ( $fields as $field => $options )
          $this->forms->field_html( 'du_forgot_password_', $field, $options, $this->current_value[ $field ] );
        if ( !empty( $this->modal ) ) : ?>
        <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
        <?php endif; ?>
        <input type="hidden" name="current_url" value="<?php echo $current_url; ?>" />
        <input type="hidden" name="<?php echo 'du_forgot_password_'; ?>nonce" value="<?php echo wp_create_nonce( 'du-forgot-password-nonce' ); ?>"/>
        <span class="form-actions">
          <button type="submit" class="button" value="">
            <span class="icon">
              <i class="fa fa-key"></i>
            </span>
            <?php _e('Reset Password'); ?>
          </button>
        </span>
      
      <?php
    }  

    function reset_password( $redirect='' ) {
      
      if ( empty( $_REQUEST[ 'action' ] ) ) :
        $is_ajax = false;
      else : 
        $is_ajax = ( $_REQUEST[ 'action' ] == 'password_reset' ? true : false );
      endif;
      
      if ( $is_ajax ) {
        if ( ! wp_verify_nonce( $_REQUEST[ 'nonce' ], 'duable' ) )
          wp_send_json_error();

        # Gather serialized form data
        $data = array();
        parse_str( $_REQUEST[ 'serialized' ], $data );

        # Save data in ajax_request variable to pass to form
        $this->ajax_request = $data;

        # Create response object
        $json_data = array(
          'nonce'   => wp_create_nonce( 'duable' )
        );
        # Add serialized data to $_POST variable
        $_POST = array_merge( $_POST, $data );
      }

      $redirect = apply_filters( 'du_forgot_password_redirect', $redirect );

      if ( isset( $_POST[ 'du_forgot_password_email_address' ]) && wp_verify_nonce( $_POST[ 'du_forgot_password_nonce' ], 'du-forgot-password-nonce' ) ) {
   
        // this returns the user ID and other info from the user name
        $user = get_user_by( 'email', $_POST[ 'du_forgot_password_email_address' ] );

        if ( $user == false ) {
          // if the user name doesn't exist
          $this->forms->errors()->add( 'du_forgot_password_email_address_invalid', $_POST[ 'du_forgot_password_email_address' ] . __(' not found.') );
        }
        
        // retrieve all error messages
        $errors = $this->forms->errors()->get_error_messages();
     
        // only send the user password if there are no errors
        if ( empty( $errors ) ) {
          $password = substr(uniqid('', true), -5);
          settype( $password, 'string' );
          wp_set_password( $password, $user->ID );
          $subject = get_bloginfo( 'name' ) . ' Password Reset';
          $content = '<html><head></head><body>';
          $content .= '<p>Hello ' . $user->first_name . ',</p>';
          $content .= '<p>You recently requested to reset your password at ' . get_bloginfo( 'url' ) . '.</p>';
          $content .= '<p>Your new password is: <strong>' . $password . '</strong></p>';
          $content .= '<p>Thank you!</p>';
          $content .= '<p><strong>The ' . get_bloginfo( 'name' ) . ' Team</strong></p>';
          $content .= '</body></html>';
          $headers = "Content-type: text/html";
          $result = wp_mail( $user->user_email, $subject, $content, $headers );
          if ( $is_ajax ) {
            $json_data[ 'password_reset_redirect' ] = $_POST[ 'current_url' ] . '?forgot_password=sent';
            wp_send_json_success( $json_data );
          } else {
            wp_redirect( $_POST[ 'current_url' ] . '?forgot_password=sent' ); exit;
          }
        }

        if ( $is_ajax ) {
          ob_start();
          $this->password_reset_form();
          $json_data[ 'password_reset_form' ] = ob_get_contents();
          ob_end_clean(); 
          wp_send_json_success( $json_data );
        }

      }
    }

    function is_modal( $modal ){
      $this->modal = $modal;
    }


  } new password_reset;
}