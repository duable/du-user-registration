/**
 * @url http://webdevstudios.com/2015/02/12/handling-ajax-in-wordpress/
 */
window.du_user_login_ajax = ( function( window, document, $ ){
  var app = {};

  app.cache = function(){
    app.$login_form = $( '.du_login_form.ajax' );
    app.$registration_form = $( '.du_registration_form.ajax' );
    app.$activation_form = $( '.du_user_activation_form.ajax' );
    app.$password_reset_form = $( '.du_forgot_password_form.ajax' );
    app.$user_settings_form = $( '.du_user_settings_form.ajax' );
  };

  app.init = function(){
    app.cache();
    $( document ).on( 'submit', '.du_login_form.ajax', app.login_form_handler );
    $( document ).on( 'submit', '.du_registration_form.ajax', app.registration_form_handler );
    $( document ).on( 'submit', '.du_forgot_password_form.ajax', app.password_reset_form_handler );
    $( document ).on( 'submit', '.du_user_activation_form.ajax', app.activation_form_handler );
    $( document ).on( 'submit', '.du_user_settings_form.ajax', app.user_settings_form_handler );
  };

  app.post_ajax = function( action_name, serial_data, response_handler ){
    var post_data = { 
      action     : action_name,
      nonce      : du_users_ajax.nonce,
      serialized : serial_data,
    };
    $.post( 
      du_users_ajax.ajax_url, 
      post_data, 
      response_handler, 
      "json" )
    .done();
  };

  app.login_form_handler = function( evt ){
    evt.preventDefault();
    var serialized_data = app.$login_form.serialize();
    app.post_ajax( 
      'user_login', 
      serialized_data, 
      app.login_response 
    );
  };

  app.login_response = function( response_data ){
    if ( response_data.success ){
        du_users_ajax.nonce = response_data.data.nonce;
        if ( response_data.data.login_redirect )
          window.location = response_data.data.login_redirect;
        app.$login_form.html( response_data.data.login_form );
        jQuery( '.alert' ).fadeIn( 500 ).css( 'display', 'block' );
    } else {
        console.log( 'Error with ajax script.' );
    }
  };

  app.registration_form_handler = function( evt ){
    evt.preventDefault();
    var serialized_data = app.$registration_form.serialize();
    app.post_ajax( 
      'register_user', 
      serialized_data, 
      app.registration_response 
    );
  };

  app.registration_response = function( response_data ){
    if ( response_data.success ){
        du_users_ajax.nonce = response_data.data.nonce;
        if ( response_data.data.registration_redirect )
          window.location = response_data.data.registration_redirect;
        app.$registration_form.html( response_data.data.registration_form );
        jQuery( '.alert' ).fadeIn( 500 ).css( 'display', 'block' );
    } else {
        console.log( 'Error with ajax script.' );
    }
  };

  app.password_reset_form_handler = function( evt ){
    evt.preventDefault();
    var serialized_data = app.$password_reset_form.serialize();
    app.post_ajax( 
      'password_reset', 
      serialized_data, 
      app.password_reset_response 
    );
  };

  app.password_reset_response = function( response_data ){
    if ( response_data.success ){
        du_users_ajax.nonce = response_data.data.nonce;
        if ( response_data.data.password_reset_redirect )
          window.location = response_data.data.password_reset_redirect;
        app.$password_reset_form.html( response_data.data.password_reset_form );
        jQuery( '.alert' ).fadeIn( 500 ).css( 'display', 'block' );
    } else {
        console.log( 'Error with ajax script.' );
    }
  };

  app.activation_form_handler = function( evt ){
    evt.preventDefault();
    var serialized_data = app.$activation_form.serialize();
    app.post_ajax( 
      'user_activation', 
      serialized_data, 
      app.activation_response 
    );
  };

  app.activation_response = function( response_data ){
    if ( response_data.success ){
        du_users_ajax.nonce = response_data.data.nonce;
        if ( response_data.data.activation_redirect )
          window.location = response_data.data.activation_redirect;
        app.$activation_form.html( response_data.data.activation_form );
        jQuery( '.alert' ).fadeIn( 500 ).css( 'display', 'block' );
    } else {
        console.log( 'Error with ajax script.' );
    }
  };

  app.user_settings_form_handler = function( evt ){
    evt.preventDefault();
    var serialized_data = app.$user_settings_form.serialize();
    app.post_ajax( 
      'update_user_settings', 
      serialized_data, 
      app.user_settings_response 
    );
  };

  app.user_settings_response = function( response_data ){
    console.log( response_data );
    if ( response_data.success ){
        du_users_ajax.nonce = response_data.data.nonce;
        if ( response_data.data.user_settings_redirect )
          window.location = response_data.data.user_settings_redirect;
        app.$user_settings_form.html( response_data.data.user_settings_form );
        jQuery( '.alert' ).fadeIn( 500 ).css( 'display', 'block' );
    } else {
        console.log( 'Error with ajax script.' );
    }
  };
  $(document).ready( app.init );
  return app;

})( window, document, jQuery );