<?php

namespace du {

  class user_settings {
  
    function __construct( $args = array() ) {

      $this->prefix = 'du_user_settings_';
      $this->redirect = '?updated_profile';

      # Get arguments
      $this->args = $args;

      # Load AJAX Handler JS
      add_action( 'wp_enqueue_scripts', array( $this, 'load_js' ) );
      
      # Process form on init
      //add_action( 'init', array( $this, 'update_settings' ) );

      # AJAX Calls
      add_action( 'wp_ajax_update_user_settings' , array( $this, 'update_settings'  ) );
      add_action( 'wp_ajax_nopriv_update_user_settings' , array( $this, 'update_settings'  ) );

      $this->forms = new forms;
    }

    function load_js() {
      wp_register_script(
        'du-users-ajax'
        , ( du_site_asset( '/vendor/duable/du-user-registration/assets/js/jquery.du-user-ajax.js' ) )
        , 'global'
        , null
        , true
      );
      wp_enqueue_script( 'du-users-ajax' );
      # Load Ajax Script
      wp_localize_script( 
        'du-users-ajax', 
        'du_users_ajax', 
        array( 
          'ajax_url'  => admin_url( 'admin-ajax.php' ),
          'nonce'     => wp_create_nonce( 'duable' )
        ) 
      );
    }

    function load_css( $css_files ) {
      $css = array( 'du-users-forms' => '/vendor/duable/du-user-registration/assets/css/du-user-login-register-forms.css' );
      array_splice( $css_files, 1, 0, $css );
      return $css_files;
    }

    function settings_form( $fields = null ) {
      if ( !is_user_logged_in() )
        return;

      global $wp;
      $current_url = home_url( add_query_arg( array(), $wp->request ) );

      if ( !empty( $this->args[ 'fields' ] ) && $this->args[ 'fields' ] != null )
        $fields = $this->args[ 'fields' ];

      # Default fields
      if ( empty( $fields ) )
        $fields = array( 
          'first_name' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "First Name",
            'show_label' => true
          ), 
          'last_name' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "Last Name",
            'show_label' => true
          ),
          'user_email' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "Email Address",
            'show_label' => true
          ), 
          /*
          'favorite_color' => array( 
            'required' => true,
            'type'     => 'select',
            'desc'     => "Favorite Color",
            'options'  => array(
              'red'   =>  'Red',
              'blue'  =>  'Blue',
              'green' =>  'Green'
            ),
            'show_label' => true
          ),
          */
        ); 

      # If ajax request data exists, replace $_POST variable
      if ( !empty( $this->ajax_request ) ) $_POST = $this->ajax_request;

      # Create a variable with existing values
      foreach ( $fields as $field => $options ) {
        $this->current_value[ $field ] = '';

        if ( ( $field != 'password' ) && ( $field != 'repeat_password' ) ) {
          $this->current_value[ $field ] = get_user_meta( get_current_user_id(), $field, true );
          $this->current_value[ $field ] = ( !empty($_POST[$this->prefix . $field] ) ? $_POST[$this->prefix . $field] : $this->current_value[ $field ] );
        }

        if ( $field == 'first_name' || $field == 'last_name' || $field == 'user_email' ) {
          $user = get_userdata( get_current_user_id() );
          $this->current_value[ $field ] = $user->{$field};
          $this->current_value[ $field ] = ( !empty($_POST[$this->prefix . $field] ) ? $_POST[$this->prefix . $field] : $this->current_value[ $field ] );
        }
      }

      # Build the form ?>
      
      <?php 
      foreach ( $fields as $field => $options )
        $this->forms->field_html( $this->prefix, $field, $options, $this->current_value[ $field ] );
      ?>
      <?php if ( !empty( $this->modal ) ) : ?>
      <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
      <?php endif; ?>
      
      <input type="hidden" name="current_url" value="<?php echo $current_url; ?>" />
      <input type="hidden" name="<?php echo $this->prefix; ?>nonce" value="<?php echo wp_create_nonce('duable'); ?>"/>

      <span class="form-actions">
        <button class="button" type="submit" value="">
          <span class="icon">
            <i class="fa fa-user"></i>
          </span>
          <?php _e('Update Settings'); ?>
        </button>
      </span>
      <?php
    }

    function update_settings() {
      # Redirect after success
      $redirect = $this->redirect;

      # Check if called by AJAX
      if ( empty( $_REQUEST[ 'action' ] ) ) :
        $is_ajax = false;
      else : 
        $is_ajax = ( $_REQUEST[ 'action' ] == 'update_user_settings' ? true : false );
      endif;

      # If called by AJAX, grab data
      if ( $is_ajax ) {

        if ( ! wp_verify_nonce( $_REQUEST[ 'nonce' ], 'duable' ) )
          wp_send_json_error();

        # Gather serialized form data
        $data = array();
        parse_str( $_REQUEST[ 'serialized' ], $data );

        # Save data in ajax_request variable to pass to form
        $this->ajax_request = $data;

        # Create response object
        $json_data = array(
          'nonce'   => wp_create_nonce( 'duable' )
        );
        # Add serialized data to $_POST variable
        $_POST = array_merge( $_POST, $data );
      }

      if ( !empty( $_POST[ $this->prefix . 'user_email' ] ) ) {
        $user_meta = array();
        $user_meta[ 'favorite_color' ] = $_POST[ $this->prefix . "favorite_color" ];
        $user_data[ 'first_name' ]  = $_POST[ $this->prefix . "first_name" ];
        $user_data[ 'last_name' ] = $_POST[ $this->prefix . "last_name" ]; 
        $user_data[ 'user_email' ] = $_POST[ $this->prefix . "user_email" ];  


        # Check for existing user by email entered
        $user = get_current_user_id();

        # ERRORS HERE

        $errors = $this->forms->errors()->get_error_messages();
        # Only update the user info if there are no errors
        if ( empty( $errors ) ) {
          

          global $wpdb;

          # First let's update the standard fields
          $update_fields = array();
          $update_fields[ 'ID' ] = $user;
          foreach ( $user_data as $key => $value ) {
            if ( !empty( $value ) ) :
              $update_fields[ $key ] = esc_attr( $value );
              # If updating email, update login to reflect change
              if ( $key == 'user_email' )
                $wpdb->update( $wpdb->users, array( 'user_login' => esc_attr( $user_data[ 'user_email' ] ) ), array( 'ID' => $user ) );
            endif;
          }
          $update = wp_update_user( $update_fields );

          # Now the meta data
          foreach ( $user_meta as $key => $value ) {
            if ( !empty( $value ) )
              update_user_meta( $user, $key, esc_attr( $value ) );
          }

          # Since we are messing with login creds, let's make sure we 
          # stay logged in!
          wp_set_current_user( $user );
          wp_set_auth_cookie( $user );
          do_action( 'wp_signon', $user_data[ 'user_email' ] );

          if ( $is_ajax ) {
            $json_data[ 'user_settings_redirect' ] = $_POST[ 'current_url' ] . '?updated_profile=true';
            wp_send_json_success( $json_data );
          } else {
            wp_redirect( $redirect ); exit;
          }

        }

        if ( $is_ajax ) {
          ob_start();
          $this->settings_form();
          $json_data[ 'user_settings_form' ] = ob_get_contents();
          ob_end_clean(); 
          wp_send_json_success( $json_data );
        }

      }

    }

  } new user_settings;

}