<?php

namespace du {
  
  /**
   * Output and process user login/registration forms
   *
   * @package default
   * @author mohammad@duable.com
   **/
  class user_registration {
    
    function __construct( $args = array() ){
      # Form field prefixes
      $this->prefix    = 'du_user_register_';

      # Registration redirect url
      $this->redirect  = '?registered';

      # Get arguments
      $this->args = $args;

      # Load AJAX Handler JS
      add_action( 'wp_enqueue_scripts', array( $this, 'load_js' ) );
      
      # Process login/register on init
      add_action( 'init', array( $this, 'register_user' ) );
      
      # AJAX Calls
      add_action( 'wp_ajax_register_user' , array( $this, 'register_user'  ) );
      add_action( 'wp_ajax_nopriv_register_user' , array( $this, 'register_user'  ) );

      $this->forms = new forms;
    }

    function load_js() {
      wp_register_script(
        'du-users-ajax'
        , ( du_site_asset( '/vendor/duable/du-user-registration/assets/js/jquery.du-user-ajax.js' ) )
        , 'global'
        , null
        , true
      );
      wp_enqueue_script( 'du-users-ajax' );
      # Load Ajax Script
      wp_localize_script( 
        'du-users-ajax', 
        'du_users_ajax', 
        array( 
          'ajax_url'  => admin_url( 'admin-ajax.php' ),
          'nonce'     => wp_create_nonce( 'duable' )
        ) 
      );
    }

    function load_css( $css_files ) {
      $css = array( 'du-users-forms' => '/vendor/duable/du-user-registration/assets/css/du-user-login-register-forms.css' );
      array_splice( $css_files, 1, 0, $css );
      return $css_files;
    }

    function registration_form( $fields = null ){
      if ( !empty( $this->args[ 'fields' ] ) && $this->args[ 'fields' ] != null )
        $fields = $this->args[ 'fields' ];

      # Default fields
      if ( empty( $fields ) )
        $fields = array( 
          'first_name' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "First Name",
            'show_label' => false
          ), 
          'last_name' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "Last Name"
          ),
          'email_address' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "Email Address"
          ), 
          'password' => array( 
            'required' => true,
            'type'     => 'password',
            'desc'     => "Password",
            'show_req' => true
          ), 
          'repeat_password' => array( 
            'required' => true,
            'type'     => 'password',
            'desc'     => "Repeat Password",
          ), 
        ); 

      # If ajax request data exists, replace $_POST variable
      if ( !empty( $this->ajax_request ) )
        $_POST = $this->ajax_request;

      # Create a variable with existing values in $_POST
      foreach ( $fields as $field => $options ) {
        $this->current_value[ $field ] = user_registration_settings::defaults( $this->prefix, $field, $options );
      }

      # Build the form ?>
      
        <?php 
        foreach ( $fields as $field => $options )
          $this->forms->field_html( $this->prefix, $field, $options, $this->current_value[ $field ] );
        ?>
        <?php if ( @$this->modal ) : ?>
        <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
        <?php endif; ?>
        <input type="hidden" name="<?php echo $this->prefix; ?>nonce" value="<?php echo wp_create_nonce('du-user-register-nonce'); ?>"/>
        <span class="form-actions">
          <button class="button" type="submit" value="">
            <span class="icon">
              <i class="fa fa-user"></i>
            </span>
            <?php _e('Register'); ?>
          </button>
        </span>
      <?php
    }  

    function register_user() {      
      $redirect = $this->redirect;

      if ( empty( $_REQUEST[ 'action' ] ) ) :
        $is_ajax = false;
      else : 
        $is_ajax = ( $_REQUEST[ 'action' ] == 'register_user' ? true : false );
      endif;

      if ( $is_ajax ) {
        if ( ! wp_verify_nonce( $_REQUEST[ 'nonce' ], 'duable' ) )
          wp_send_json_error();

        # Gather serialized form data
        $data = array();
        parse_str( $_REQUEST[ 'serialized' ], $data );

        # Save data in ajax_request variable to pass to form
        $this->ajax_request = $data;

        # Create response object
        $json_data = array(
          'nonce'   => wp_create_nonce( 'duable' )
        );
        # Add serialized data to $_POST variable
        $_POST = array_merge( $_POST, $data );
      }

      if ( isset( $_POST[ $this->prefix . 'email_address' ] ) && wp_verify_nonce( $_POST[ $this->prefix . 'nonce' ], 'du-user-register-nonce' ) ) {
        $user_email   = $_POST[$this->prefix . "email_address"];
        $user_first   = $_POST[$this->prefix . "first_name"];
        $user_last    = $_POST[$this->prefix . "last_name"];
        $user_pass    = $_POST[$this->prefix . "password"];
        $pass_confirm = $_POST[$this->prefix . "repeat_password"];
        $user_login = $user_email;

        # Check for existing user by email entered
        $user = get_user_by( 'email', $user_email );

        # Username already registered
        if ( !empty( $user ) )
          $this->forms->errors()->add( $this->prefix . 'email_address_unavailable', $user_email . __(' is already registered.' ) );

        # Empty username
        if ( $user_login == '' )
          $this->forms->errors()->add( $this->prefix . 'email_address_empty', __('Please enter your email address'));
        # Invalid email
        if ( !is_email( $user_email ) ) 
          $this->forms->errors()->add( $this->prefix . 'email_address_invalid', __('Invalid email'));
        
        # No password entered
        if( $user_pass == '' ) 
          $this->forms->errors()->add( $this->prefix . 'password_empty', __('Please enter a password'));
        
        # Passwords do not match
        if( $user_pass != $pass_confirm ) 
          $this->forms->errors()->add( $this->prefix . 'repeat_password_mismatch', __('Passwords do not match'));

        # Passwords under 8 characters
        if ( strlen($user_pass) < 8 )
          $this->forms->errors()->add( $this->prefix . 'password_less_than_8', __('Password must be at least 8 characters long'));

        # Password at least one uppercase character
        if ( !preg_match( '`[A-Z]`', $user_pass ) )
          $this->forms->errors()->add( $this->prefix . 'password_', __('Password must contain at least one capital letter'));

        # Password at least one number
        if ( !preg_match( '`[0-9]`', $user_pass ) )
          $this->forms->errors()->add( $this->prefix . 'password_no_number', __('Password must contain at least one number'));

        $errors = $this->forms->errors()->get_error_messages();
     
        # Only create the user in if there are no errors
        if ( empty( $errors ) ) {
          $new_user_id = wp_insert_user(array(
              'user_pass'       => $user_pass,
              'user_email'      => $user_email,
              'first_name'      => $user_first,
              'last_name'       => $user_last,
              'user_registered' => date('Y-m-d H:i:s'),
              'role'            => 'subscriber',
              'user_login'      => $user_email,
            )
          );

          if ( $new_user_id ) {
            # Send an email to the admin alerting them of the registration
            wp_new_user_notification( $new_user_id );
            
            # Generate activation key and begin activation process
            $activate = new \du_user_activation( $new_user_id );
            $activate->generate_key();

            # Log the new user in
            wp_set_current_user( $new_user_id );
            wp_set_auth_cookie( $new_user_id );
            $new_user = get_user_by( 'id', $new_user_id );
            do_action( 'wp_signon', $new_user->user_login );
            
            if ( $is_ajax ) {
              $json_data[ 'registration_redirect' ] = home_url() . $redirect;
              wp_send_json_success( $json_data );
            } else {
              wp_redirect( home_url() . $redirect );
            }

          }
        }

        if ( $is_ajax ) {
          ob_start();
          $this->registration_form();
          $json_data[ 'registration_form' ] = ob_get_contents();
          ob_end_clean(); 
          wp_send_json_success( $json_data );
        }

      }

    }

    function is_modal( $modal ){
      $this->modal = $modal;
    }


  } new user_registration;
}