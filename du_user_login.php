<?php

namespace du {

  /**
   * Output and process user login/registration forms
   *
   * @package default
   * @author mohammad@duable.com
   **/
  class user_login {
    
    function __construct( $args = array() ){
      # Form field prefixes
      $this->prefix           = 'du_user_login_';

      # Logged in redirect url
      $this->redirect = '?loggedin';
      $this->redirect = apply_filters( 'du_login_redirect', $this->redirect );

      # Get arguments
      $this->args = $args;

      # Load AJAX Handler JS
      add_action( 'wp_enqueue_scripts', array( $this, 'load_js' ) );
      
      # Process login/register on init
      add_action( 'init', array( $this, 'login_user' ) );
      
      # AJAX Calls
      add_action( 'wp_ajax_login_user' , array( $this, 'login_user'  ) );
      add_action( 'wp_ajax_nopriv_login_user' , array( $this, 'login_user'  ) );

      $this->forms = new forms;
    }

    function load_js() {
      wp_register_script(
        'du-users-ajax'
        , ( du_site_asset( '/vendor/duable/du-user-registration/assets/js/jquery.du-user-ajax.js' ) )
        , 'global'
        , null
        , true
      );
      wp_enqueue_script( 'du-users-ajax' );
      # Load Ajax Script
      wp_localize_script( 
        'du-users-ajax', 
        'du_users_ajax', 
        array( 
          'ajax_url'  => admin_url( 'admin-ajax.php' ),
          'nonce'     => wp_create_nonce( 'duable' )
        ) 
      );
    }

    function load_css( $css_files ) {
      $css = array( 'du-users-forms' => '/vendor/duable/du-user-registration/assets/css/du-user-login-register-forms.css' );
      array_splice( $css_files, 1, 0, $css );
      return $css_files;
    }

    function login_form( $fields=null ){
      if ( is_user_logged_in() )
        die();

      # Default fields
      if ( empty( $fields ) )
        $fields = array( 
          'email_address' => array( 
            'required' => true,
            'type'     => 'text',
            'desc'     => "Email Address"
          ),
          'password' => array( 
            'required' => true,
            'type'     => 'password',
            'desc'     => "Password"
          ),
        ); 

      # If ajax request data exists, replace $_POST variable
      if ( !empty( $this->ajax_request ) )
        $_POST = $this->ajax_request;
      
      # Create a variable with existing values in $_POST
      foreach ( $fields as $field => $options ) {
        $this->current_value[ $field ] = user_login_settings::defaults( $this->prefix, $field, $options );
      }

      # Build the form ?>
      
        <?php 
        foreach ( $fields as $field => $options )
          $this->forms->field_html( $this->prefix, $field, $options, $this->current_value[ $field ] );
        if ( !empty( $this->modal ) ) : ?>
        <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
        <?php endif; ?>
        <input type="hidden" name="<?php echo $this->prefix; ?>nonce" value="<?php echo wp_create_nonce( 'duable' ); ?>"/>
        <span class="form-actions">
          <button type="submit" class="button" value="">
            <span class="icon">
              <i class="fa fa-sign-in"></i>
            </span>
            <?php _e('Login'); ?>
          </button>
          <br />
          <a class="forgot-password" <?php du_modal( 'forgot-password' ); ?>>Forgot your password?</a>
        </span>
      <?php
    }  

    function login_user( $redirect = '' ) {
      if ( empty( $_REQUEST[ 'action' ] ) ) :
        $is_ajax = false;
      else : 
        $is_ajax = ( $_REQUEST[ 'action' ] == 'user_login' ? true : false );
      endif;
      
      if ( $is_ajax ) {
        if ( ! wp_verify_nonce( $_REQUEST[ 'nonce' ], 'duable' ) )
          wp_send_json_error();

        # Gather serialized form data
        $data = array();
        parse_str( $_REQUEST[ 'serialized' ], $data );

        # Save data in ajax_request variable to pass to form
        $this->ajax_request = $data;

        # Create response object
        $json_data = array(
          'nonce'   => wp_create_nonce( 'duable' )
        );
        # Add serialized data to $_POST variable
        $_POST = array_merge( $_POST, $data );
      }

      if ( isset( $_POST[ $this->prefix . 'email_address' ] ) 
        && wp_verify_nonce($_POST[ $this->prefix . 'nonce' ], 'duable' ) 
        ) {
   
        # This returns the user ID and other info from the user name
        $user = get_user_by( 'email', $_POST[ $this->prefix . 'email_address' ] );
        
        # User errors
        if ( !empty( user_login_settings::errors( $this->prefix, $user ) ) ) :
          foreach ( user_login_settings::errors( $this->prefix, $user ) as $code => $message ) {
            $this->forms->errors()->add( $code, __( $message ) );
          }
        endif;
     
        # Retrieve all error messages
        $errors = $this->forms->errors()->get_error_messages();
     
        # Only log the user in if there are no errors
        if ( empty( $errors ) ) {
          wp_set_current_user( $user->ID );
          wp_set_auth_cookie( $user->ID );
          do_action( 'wp_signon', $user->user_login );
          if ( $is_ajax ) {
            $json_data[ 'login_redirect' ] = home_url() . $redirect;
            wp_send_json_success( $json_data );
          } else {
            wp_redirect( home_url() . $redirect ); exit;
          }
        }

        if ( $is_ajax ) {
          ob_start();
          $this->login_form();
          $json_data[ 'login_form' ] = ob_get_contents();
          ob_end_clean(); 
          wp_send_json_success( $json_data );
        }

      }
    }

    function is_modal( $modal ){
      $this->modal = $modal;
    }


  } new user_login;
}