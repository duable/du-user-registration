<?php

/**
 * Handle email verification to activate accounts
 *
 * @package default
 * @author mohammad@duable.com
 **/
class du_user_activation {
  
  function __construct( $user=null ){

    # If no user is set, then use current user
    $this->user = ( empty( $user ) ? get_current_user_id() : $user );

    # Option name for activation key meta
    $this->key_meta_name = 'du_activation_key';

    # Option name for activation status meta
    $this->active_meta_name = 'du_activation_status';

    # Form data
    add_action( 'init', array( $this, 'check_key' ) );
    add_action( 'init', array( $this, 'resend_activation' ) );
    add_action( 'init', array( $this, 'manual_activate' ) ); 

    # AJAX Calls
    add_action( 'wp_ajax_user_activation' , array( $this, 'check_key'  ) );
    add_action( 'wp_ajax_nopriv_user_activation' , array( $this, 'check_key'  ) );

    $this->forms = new du\forms;

  }


  function is_active() {
    # Default status
    $active = false;

    # Check if user is activated already
    $activated = get_user_meta( $this->user, $this->active_meta_name, true );
    if ( $activated == 'true' )
      $active = true;

    # If activation key meta field doesn't exist, then treat as active user
    $void = get_user_meta( $this->user, $this->key_meta_name, true );
    if ( empty( $void ) )
      $active = true;

    # Never question the super admins
    if ( is_super_admin() )
      $active = true;

    return $active;
  }

  function generate_key(){
    # Get user object
    $user = get_user_by( 'id', $this->user );

    # Make sure user is set as inactive
    @update_user_meta( $this->user, $this->active_meta_name, 'false' );
    
    # Variable comparing current and new keys
    $same = false;

    # Get user activation key if it exists
    $current_key = get_user_meta( $this->user, $this->key_meta_name, true );

    # Generate random key with 5 characters
    # We use 5 chars to make it easy to remember and type in manually
    $key = substr( uniqid( '', true ), -5 );

    # If keys are the same, leep changing key until they are different
    while( $current_key == $key ) {
      $key = substr( uniqid( '', true ), -5 );
      if ( $current_key != $key )
        break;
    }

    # Update key value for user
    $updated = update_user_meta( $this->user, $this->key_meta_name, $key );

    # Log updated key
    if ( !$updated ) :
      $log  =  '[' . date("F j, Y, g:i a") . '] ' . 'Error: Unable to update activation key for ' . $user->user_email . '.'.PHP_EOL;
    else :
      $log  =  '[' . date("F j, Y, g:i a").  '] ' . 'Success: Updated activation key for ' . $user->user_email . '.'.PHP_EOL;
    endif;
    
    du_write_log( $log );

    # Send email notification to user
    $subject = get_bloginfo( 'name' ) . ' Account Activation';
    $body = 'Hello ' . $user->first_name . ', 
      You recently created an account at ' . get_bloginfo( 'url' ) .'.

      Your activation key is ' . $key . '. 

      You can click the following link to activate your account automatically: ' . get_bloginfo( 'url' ) . '?activation_key=' . $key. '&activation_email=' . $user->user_email . '.

      Thank you for registering,

      ' . du_email_settings::name();

    $result = wp_mail( $user->user_email, $subject, $body );
  }


  function activation_form( $fields = null ){
    # Default fields
    if ( empty( $fields ) )
      $fields = array( 
        'activation_key' => array( 
          'required' => true,
          'type'     => 'text',
          'desc'     => "Enter your activation key"
        ), 
      ); 

    # Create a variable with existing values in $_POST
    foreach ( $fields as $field => $options ) {
  
      $this->current_value[ $field . '_current' ] = '';
      if ( ( $field != 'password' ) && ( $field != 'repeat_password' ) ) {
        $this->current_value[ $field . '_current' ] = ( !empty($_POST['du_user_activation_' . $field] ) ? $_POST['du_user_activation_' . $field] : '' );
      }

    }

    # Build the form ?>
    
    <?php 
    foreach ( $fields as $field => $options )
      $this->forms->field_html( 'du_user_activation_', $field, $options, $this->current_value[ $field . '_current' ] );
    ?>
    <?php if ( !empty( $this->modal ) ) : ?>
    <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
    <?php endif; ?>
        <input type="hidden" name="du_user_activation_nonce" value="<?php echo wp_create_nonce( 'du-user-activation-nonce' ); ?>"/>
        <button class="button" action="submit"><?php _e('Activate My Account'); ?></button>
      </span>
    <?php
  }  

  function manual_activation( $fields = null ){
    # Default fields
    if ( empty( $fields ) )
      $fields = array( 
        'user_email' => array( 
          'required' => true,
          'type'     => 'text',
          'desc'     => "Enter the user's email address"
        ), 
      ); 

    # Create a variable with existing values in $_POST
    foreach ( $fields as $field => $options ) {
  
      $this->current_value[ $field . '_current' ] = '';
      if ( ( $field != 'password' ) && ( $field != 'repeat_password' ) ) {
        $this->current_value[ $field . '_current' ] = ( !empty($_POST['du_user_activation_' . $field] ) ? $_POST['du_user_activation_' . $field] : '' );
      }

    }

    # Build the form ?>
    <form id="du_manual_activation_form" class="du_manual_activation_form" action="" method="POST">
      <?php 
      foreach ( $fields as $field => $options )
        $this->forms->field_html( 'du_user_activation_', $field, $options, $this->current_value[ $field . '_current' ] );
      ?>
      <?php if ( @$this->modal ) : ?>
      <input type="hidden" name="du_open_modal" value="<?php echo $this->modal; ?>"/>
      <?php endif; ?>
      <input type="hidden" name="du_manual_activation_nonce" value="<?php echo wp_create_nonce( 'du-manual-activation-nonce' ); ?>"/>
      <button class="button" action="submit"><?php _e('Activate Account'); ?></button>
    </form>
    <?php
  }  

  function resend_activation() {
    # Resend activation link clicked
    if ( !empty( $_GET[ 'du_resend_activation'] ) ) {
      $this->user = get_current_user_id();
      
      $user = get_user_by( 'id', $this->user );

      $this->generate_key();

      # Log activity
      $log  =  '[' . date("F j, Y, g:i a").  '] ' . 'Activation email resent to ' . $user->user_email . '.'.PHP_EOL;

      du_write_log( $log );

      header( "Location: " . home_url( '?active=resend' ) );
    }
  }

  function check_key(){

    # Activation link clicked from email
    if ( !empty( $_GET[ 'activation_key'] ) && !empty( $_GET[ 'activation_email' ] ) ) {

      $user = get_user_by( 'email', $_GET[ 'activation_email' ] );
      
      $current_key = get_user_meta( $user->ID , $this->key_meta_name, true );
      
      if ( $_GET[ 'activation_key' ] == $current_key )
        $this->activate_account( $user->ID );
        
      return;
    }

    if ( empty( $_REQUEST[ 'action' ] ) ) :
      $is_ajax = false;
    else : 
      $is_ajax = ( $_REQUEST[ 'action' ] == 'user_activation' ? true : false );
    endif;

    if ( $is_ajax ) {
      $this->is_ajax = true;
      if ( !wp_verify_nonce( $_REQUEST[ 'nonce' ], 'duable' ) )
        wp_send_json_error();

      # Gather serialized form data
      $data = array();
      parse_str( $_REQUEST[ 'serialized' ], $data );

      # Save data in ajax_request variable to pass to form
      $this->ajax_request = $data;

      # Create response object
      $json_data = array(
        'nonce'   => wp_create_nonce( 'duable' )
      );
      # Add serialized data to $_POST variable
      $_POST = array_merge( $_POST, $data );
    }

    # Logged in user activating account
    if ( isset( $_POST[ 'du_user_activation_activation_key' ] ) && wp_verify_nonce( $_POST[ 'du_user_activation_nonce' ], 'du-user-activation-nonce' ) ) {

      $current_key = get_user_meta( $this->user, $this->key_meta_name, true );

      if ( $current_key == $_POST[ 'du_user_activation_activation_key' ] ) :
        $this->activate_account( get_current_user_id() );
      else :
        $this->forms->errors()->add( 'du_user_activation_activation_key_invalid', __( 'Your activation key is invalid.' ) );
      endif;
      if ( $is_ajax ) {
        ob_start();
        $this->activation_form();
        $json_data[ 'activation_form' ] = ob_get_contents();
        ob_end_clean(); 
        wp_send_json_success( $json_data );
      }
    }

  }  

  function manual_activate(){
    # Manually activate user processing
    if ( !empty( $_POST[ 'du_manual_activation_user_email' ] ) ) {
      $user = get_user_by( 'email', $_POST[ 'du_manual_activation_user_email' ] );
      $this->activate_account( $user->ID ); 
      return;
    }

  }  

  function activate_account( $user ) {
    delete_user_meta( $user, $this->key_meta_name );
    update_user_meta( $user, $this->active_meta_name, 'true');
    $user_email = get_user_by( 'id', $user );
    $user_email = $user_email->user_email;

    # Log activity
    $log  =  '[' . date("F j, Y, g:i a").  '] ' . 'Success: Account activated for ' . $user_email . '.'.PHP_EOL;

    du_write_log( $log );
    
    if ( $this->is_ajax ) {
      $json_data[ 'activation_redirect' ] = home_url() . '?active=true';
      wp_send_json_success( $json_data );
    } else {
      header( "Location: " . home_url( '?active=true' ) );
    }
  }

} new du_user_activation;

function du_activation_form() {
  echo '<form id="du_user_activation_form" class="du_user_activation_form ajax" action="" method="POST">';
  $activate = new du_user_activation;
  $activate->activation_form();
  echo '</form>';
}

function du_active_user(){
  $active = false;
  $id = get_current_user_id();

  $user = new du_user_activation( $id );
  if ( $user->is_active() )
    $active = true;

  if ( is_super_admin() )
    $active = true;

  return $active;
}